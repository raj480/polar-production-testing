int numSamples=0;
long t, t0;
uint16_t tenBitValue=0;
int sensorValue = 0;
float voltage=0.0000;
float mVoltage=0.0000;
int currentVal=0;
int preVal=0;
int reserveVal=0;

#define ACTION_RESET "R"
bool resetChk = false;
char readData;

void setup()
{
  Serial.begin(19200);
  Serial.print("Setup");

//  ADCSRA = 0;             // clear ADCSRA register
  ADCSRB = 0;             // clear ADCSRB register
  ADMUX |= (0 & 0x07);    // set A0 analog input pin
//  ADMUX |= (1 << REFS1);  // set reference voltage
//  ADMUX |= (1 << REFS0);  // set reference voltage
  ADMUX |= (1 << ADLAR);  // left align ADC value to 8 bits from ADCH register

  // sampling rate is [ADC clock] / [prescaler] / [conv ersion clock cycles]
  // for Arduino Uno ADC clock is 16 MHz and a conversion takes 13 clock cycles
  //ADCSRA |= (1 << ADPS2) | (1 << ADPS0);    // 32 prescaler for 38.5 KHz
  ADCSRA |= (1 << ADPS2);                     // 16 prescaler for 76.9 KHz
  //ADCSRA |= (1 << ADPS1) | (1 << ADPS0);    // 8 prescaler for 153.8 KHz

  ADCSRA |= (1 << ADATE); // enable auto trigger
  ADCSRA |= (1 << ADIE);  // enable interrupts when measurement complete
  ADCSRA |= (1 << ADEN);  // enable ADC
  ADCSRA |= (1 << ADSC);  // start ADC measurements
  analogReference(EXTERNAL);
readVoltage();
}

ISR(ADC_vect)
{
//  byte x = ADCH;  // read 8 bit value from ADC
//  byte y = ADCL;
//  byte x = ADCH;
uint8_t theLow=ADCL;
tenBitValue=ADCH<<2 | theLow>>6;
//  Serial.print("x=");
//  Serial.println(tenBitValue);
//  Serial.print("y=");
//  Serial.println(y);
  numSamples++;
}

void readVoltage(){
  preVal = 0;
  int maxVal =0;
  for(int i=0; i<10; i++){
  unsigned long nSamples=0;
  unsigned long avgVal=0;
  unsigned long sumVal=0;
  unsigned long startMillis=millis();
//   preVal = 0;


int curVal=0;
  while(millis()-startMillis<2000){
  nSamples++;  
  sensorValue = tenBitValue;
//  Serial.println(sensorValue);
  sumVal+=sensorValue;
  avgVal=sumVal/nSamples;
  }
  voltage=3.330*(avgVal/1024.000)+0.015;
  int rVoltage=round(voltage*1000);
//  currentVal=rVoltage;
//  reserveVal=rVoltage;
//  if(currentVal>preVal){
//    currentVal=reserveVal;
//    preVal=currentVal;
//    }
  curVal = rVoltage;
  if (curVal > maxVal){
    maxVal = rVoltage;
  }
  
//  currentVal=rVoltage;
//  Serial.println(sumVal);
//  Serial.println(nSamples);
//  Serial.println(avgVal);
//  Serial.println(voltage,5);
//  Serial.println(rVoltage);
//  Serial.println(rVoltage);
//Serial.println(reserveVal);
  }
  char mVoltage[7];
  memset(mVoltage,0,7);
  sprintf(mVoltage,"%s%04d%s","$",maxVal, "@");
Serial.print("**");
Serial.print(mVoltage);
Serial.println("****");
  
  }

void software_Reset() // Restarts program from beginning but does not reset the peripherals and registers
{
//  digitalWrite(LEDpin, HIGH);
  delay(5);
//  digitalWrite(LEDpin,LOW);
asm volatile ("  jmp 0");  
}
void loop()
{
//  Serial.println("*********************************");
  if(Serial.available()>0){
    readData=Serial.read();
//    Serial.print(readData);
    if(!strncmp(readData, ACTION_RESET, 1)){
//      if(!strncmp(readData, ACTION_RESET, 3)){
      Serial.println("reset");
      resetChk=true;
      software_Reset();
      }
    else{
//      Serial.println("resetFail");
      }
//    Serial.println(readData);
    readData="0";
    }
}
