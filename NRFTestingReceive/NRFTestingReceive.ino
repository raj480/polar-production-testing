
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <EEPROM.h>
#include <IRremote.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <batData.h>

uint8_t val=0;
RF24 radio(10, 9); // CE, CSN
IRsend irsend;
const uint8_t hubAddress[6] = "I0670";
int rcvCount = 0;
bool rcvd=false;
uint8_t pseudoChar=1;

void setup() {
  Serial.begin(19200);
  
  radio.begin();
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(100);
  radio.setRetries(1, 1);
  radio.setPayloadSize(4);
  //modeSetup();

  radio.openReadingPipe(0, hubAddress);
  radio.startListening();
  
  

}


//val = SPI.transfer(0xff); //NOP
//regVal= radio.read_register(0xff);
int tst =0;
char data[32] = "";
int reg = 0;

void loop() {
  radio.openReadingPipe(0, hubAddress);
  radio.startListening();
//Serial.println(radio.isChipConnected());
//Serial.println(radio.getDataRate());
//Serial.println(radio.getPALevel());
//Serial.println(radio.getChannel());
//reg = radio.get_status();
  unsigned long u_time = micros();
  while (micros() - u_time < 17000) {
if (radio.available()) {
  radio.read(&data, sizeof(data));
//  Serial.println(data);
  rcvd = true;
  rcvCount++;
  break;
  
  }
  }
  if(rcvd){
//    Serial.println("rcvDATA");
   radio.openWritingPipe(hubAddress);
   radio.stopListening();
    char *sendMsg;
    sendMsg="K";
    int msgSize= strlen(sendMsg);  
    char tempStr[5]="";
  if (msgSize<=2) {
    for(int i=0;i<msgSize;i++){
      tempStr[i] = sendMsg[i];
    }
    tempStr[msgSize]='\0';
    tempStr[msgSize+1] = pseudoChar++;
    msgSize+=2;
    sendMsg = tempStr;
  }

 int count = radio.write(sendMsg, sizeof(sendMsg));
   if(count){
    Serial.println("rplySent");
    }
 rcvd = false;
 count=false;
    }


}
