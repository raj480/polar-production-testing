
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <EEPROM.h>
#include <IRremote.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <batData.h>

#define ACTION_RPLY     "K"

#define LEDpin 13
#define ACTION_RESET "R"
bool resetChk = false;
char readData;


RF24 radio(10, 9); // CE, CSN
IRsend irsend;
const uint8_t hubAddress[6] = "I0670";
uint8_t pseudoChar=1;
bool rcvd=false;
int snt=0;
int r=0;
int cnt=0;
int tst =0;
int crc = 0;

void setup() {
  Serial.begin(115200);
  Serial.println("SETUP");
//  pinMode(LEDpin, OUTPUT);
//  digitalWrite(LEDpin, LOW);
  radio.begin();
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(100);
  radio.setRetries(1, 1);
  radio.setPayloadSize(4);
  radio.openWritingPipe(hubAddress);
  radio.stopListening();
//  if(resetChk){
    Serial.println("nrfTest");
//    nrfTesting();
    resetChk=false;
//    }
  

}


void resultPrint(){
          int snt=960;
        int r = 960;
        char cData[10];
        Serial.print("$");
        sprintf(cData,"%s%04d%s%04d%s", "#", snt, "," , r, "@");
        Serial.print("@");
//  Serial.print("#");    
//  Serial.print(snt);
//  Serial.print(",");    
//  Serial.print(r);
//  Serial.print("@"); 
Serial.print(cData); 
  r=0;
  snt=0;
  cnt=0;
  }

  void software_Reset() // Restarts program from beginning but does not reset the peripherals and registers
{
//  digitalWrite(LEDpin, HIGH);
  delay(5);
//  digitalWrite(LEDpin,LOW);
asm volatile ("  jmp 0");  
}

inline boolean serialWaiting(){
      while(Serial.available()<=0 )
      {
        delay(4);
      }
      return true;
}

void nrfTesting(){
//    Serial.println("loop");
for(int i=0; i<1000; i++){
    cnt++;
    rcvd=false;
    radio.openWritingPipe(hubAddress);
    radio.stopListening();
    char *sendMsg;
    sendMsg="T";
    int msgSize= strlen(sendMsg);  
    char tempStr[5]="";
  if (msgSize<=2) {
    for(int i=0;i<msgSize;i++){
      tempStr[i] = sendMsg[i];
    }
    tempStr[msgSize]='\0';
    tempStr[msgSize+1] = pseudoChar++;
    msgSize+=2;
    sendMsg = tempStr;
  }

 int count = radio.write(sendMsg, sizeof(sendMsg));
   
  if(count){
    snt++;
//    Serial.println("dataSent");
    char data[32] = "";
  radio.openReadingPipe(0, hubAddress);
  radio.startListening();
  unsigned long u_time = micros();
  while (micros() - u_time < 17000) {
  if (radio.available()) {
  radio.read(&data, sizeof(data));
  //Serial.println(data);
     rcvd = true;
      if(rcvd){
         if (!strncmp(data, ACTION_RPLY, 1)) { 
           r++;
         }
       
      }

  count=false;
  break;
     }
    }
   }
   delay(5);
  }
      if(cnt==1000){
        resultPrint();
//        int snt=213;
//        int r = 567;
//  Serial.print("#");    
//  Serial.print(snt);
//  Serial.print(",");    
//  Serial.print(r);
//  Serial.print("@"); 
//  r=0;
//  snt=0;
//  cnt=0;
    }
//    delay(100);
  }

void loop() {

//  Serial.println("*********************************");
  if(Serial.available()>0){
    readData=Serial.read();
    if(!strncmp(readData, ACTION_RESET, 1)){
//      if(!strncmp(readData, ACTION_RESET, 3)){
      Serial.println("reset");
      resetChk=true;
      software_Reset();
      }
    else{
      Serial.println("resetFail");
      }
    Serial.println(readData);
    readData="0";
    }
}
